name := "log4j_in_scala"

version := "1.0.0"

scalaVersion := "2.12.7"

libraryDependencies += "log4j" % "log4j" % "1.2.14"

mainClass in assembly := Some("log")

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = true, includeDependency = true)

assemblyMergeStrategy in assembly := {
  case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
  case PathList("META-INF", "services", "org.apache.hadoop.fs.FileSystem") => MergeStrategy.filterDistinctLines
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case "application.conf"            => MergeStrategy.concat
  case "reference.conf"            => MergeStrategy.concat
  case x => MergeStrategy.last
}
