import org.apache.log4j.{Logger, PropertyConfigurator}

object logging{
  PropertyConfigurator.configure(s"${System.getProperty("user.dir")}/configs/log4j.properties")

  val log = Logger.getLogger(this.getClass.getName)

  def main(args: Array[String]): Unit = {
    log.error("Error log")
    log.warn("Warn log")
    log.info("Info log")
    log.debug("Debug log")
    log.trace("Trace log")

  }
}